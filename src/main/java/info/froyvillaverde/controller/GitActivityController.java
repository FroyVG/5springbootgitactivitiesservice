package info.froyvillaverde.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import info.froyvillaverde.service.GitApiServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class GitActivityController {

	@Autowired
	GitApiServiceImpl gitApiService;
	
	@ApiOperation(value = "Creates repos with the statistics of the given user related reponame", response = Object.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Succesfully retrieved repos", response = Object.class)})
	@RequestMapping(value = "value = /gitrepositories/user/{username}/repos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getAndPublishGitRepositoryStatisticsInDataBase(
			@ApiParam(value = "owner-name") @PathVariable(name = "user-name") String ownerName,
			@ApiParam(value = "clientToken", defaultValue = "") @RequestParam(name = "token", defaultValue = "12345") String token){
		ResponseEntity<Object> responseEntity = null;
		
		gitApiService.getAndPublishGitRepositoryStatisticsInDataBase(ownerName, token);
		responseEntity = new ResponseEntity<>("Succesfully retrieved repos and saved into database", HttpStatus.OK);
		
		return responseEntity;
	}
	
	
	@ApiOperation(value = "get all repos", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "succesfully retrieved repos", response = Object.class)})
	@RequestMapping(value = "value = /gitAllRepositories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getAllRepos(){
	ResponseEntity<Object> responseEntity = null;
	
	responseEntity = new ResponseEntity<>(gitApiService.getAllRepos(), HttpStatus.OK);
	
	return responseEntity;
	}
	
}
