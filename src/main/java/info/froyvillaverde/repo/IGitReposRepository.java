package info.froyvillaverde.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import info.froyvillaverde.entities.RepoDetails;

@Repository
public interface IGitReposRepository extends JpaRepository<RepoDetails, Long>{

}
