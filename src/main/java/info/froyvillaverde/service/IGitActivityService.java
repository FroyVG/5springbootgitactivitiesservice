package info.froyvillaverde.service;

import java.util.List;

import info.froyvillaverde.entities.RepoDetails;

public interface IGitActivityService {

	public void getAndPublishGitRepositoryStatisticsInDataBase(String userName,String token);
	
	public List<RepoDetails> getAllRepos();
	
}
